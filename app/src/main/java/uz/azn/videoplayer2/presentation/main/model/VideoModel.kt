package uz.azn.videoplayer2.presentation.main.model

data class VideoModel(
    val id: Int,
    val downloadId: Long,
    val fileTitle:String,
    val filePath: String,
    val progress: String,
    var status: String,
    val fileSize: String,
    var isPaused: Boolean
) {
}